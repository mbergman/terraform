output "instance_ip_addr" {
  value = aws_instance.MyApp.*.public_ip
}

output "instance_ip_addr_private" {
  value = aws_instance.MyApp.*.private_ip
}


output "instance_state" {
  value = aws_instance.MyApp.instance_state
}
