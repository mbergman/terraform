variable "access_key" {
        description = "The access key of your AWS account"
}

variable "secret_key" {
        description = "The secret key of your AWS account"
}
variable "instance_name" {
        description = "Name of the EC2 instance"
}
provider "aws" {
        access_key = var.access_key
        secret_key = var.secret_key
        region = "us-west-1"
}



resource "aws_instance" "MyApp" {
        ami ="ami-012f8fcc06515f35c"
        instance_type = "t2.micro"
        key_name = "CASkeyPair"
        security_groups= ["default"]
        tags          = {
                Name        = var.instance_name
        }
 
}
